# Chọn image aspnet:6.0 làm image cơ sở
FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80

# Chọn image sdk:6.0 làm image để build
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src

# Copy csproj và thực hiện restore trước để tận dụng cache
COPY ["BackEnd.csproj", "BackEnd/"]
RUN dotnet restore "BackEnd/BackEnd.csproj"

# Copy toàn bộ source code và build ứng dụng
WORKDIR "/src/BackEnd"
COPY . .
RUN dotnet build "BackEnd.csproj" -c Release -o /app/build

# Tạo image để publish ứng dụng
FROM build AS publish
RUN dotnet publish "BackEnd.csproj" -c Release -o /app/publish /p:UseAppHost=false

# Chọn image aspnet:6.0 làm image cuối cùng
FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS final
WORKDIR /app

# Copy ứng dụng từ image publish
COPY --from=publish /app/publish .

# Thiết lập lệnh chạy ứng dụng khi container được khởi chạy
ENTRYPOINT ["dotnet", "BackEnd.dll"]
